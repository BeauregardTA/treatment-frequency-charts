# First, we import libraries with functions that make our code shorter and cleaner
import pandas as pd
import sys
from sqlalchemy import create_engine
import sqlite3
import matplotlib.pyplot as plt
import os

# Set the 'newData' variable below to the absolute path to the file with your new data for the month
newData = "C:\\Users\user\Downloads\data.xlsx"

# You can set the variable below to the absolute path to your main data file if needed
e = create_engine('sqlite:///C:\\Users\user\Desktop\Charts\TxFreqData.db', echo=False)
a = create_engine('sqlite:///C:\\Users\user\Desktop\Charts\TxFreqDataArchive.db', echo=False)

# Last we need an absolute path to where you want the charts to be saved.
# chrtPath = "C:\\Users\user\Desktop\Charts"
chrtPath = os.path.abspath("C:\\Users\user\Desktop\Charts")

# This variable should be the number of months you want to be included in the chart but it has to be in quotes
num = '12'

# Each location that you want a graph for needs to be added to the dictionary below
# The name of the location as it appears in the data file first, then how you want it named in the graph
# If you want different locations always grouped together, this is the place to do that
flocations = {
    'R9E': 'R9E',
    'R9W': 'R9W',
    'R10': 'R10',
    'R11E': 'E11R',
    'R11W': 'W11R',
    'R8E': 'R8E',
    'R8W': 'R8W',
    'C11A': 'C11',
    'C11B': 'C11',
    'C11E': 'C11',
    'C11F': 'C11',
    'C10A': 'C10',
    'C10B': 'C10',
    'C10E': 'C10',
    'C10F': 'C10',
    'R7E': 'R7E'
}

#Python does weird things and is really picky. We need our locations as a list so we can work with it in different ways.
locationsList = list(set(flocations.values()))

# Similar to the above, if you want to make combined bins for treatment frequency
# or change their names on the chart this is the place to do so.
Frequency = {
    'no therapy warranted': '0x/wk',
    '1 time a week': '1x/wk',
    '2 times a week': '2-3x/wk',
    '3 times a week': '2-3x/wk',
    '4 times a week': '4x/wk',
    '5 times a week': '5x/wk',
    '6 times a week': '6-7x/wk',
    '7 times a week': '6-7x/wk',
    '2 times a day': '6-7x/wk'
}
freqList = list(set(Frequency.values()))
freqList.sort()

# Currently, PowerBI most wants to give us an excel file, so we read the file in that format.
# We return an error and stop the program if there's a problem reading the file
try:
    raw = pd.read_excel(newData, parse_dates=[4])
except FileNotFoundError:
    sys.exit('The new data couldn\'t be found. Is it in the right place? Does it have the right name?')
except:
    sys.exit('There was a problem reading the new data. Exiting')

# I'm going to remove columns that we don't need. It probably won't make a difference but, hypothetically,
# we'll use less memory and it'll run faster?
raw.drop(raw.columns[[0, 2, 3, 5, 6, 7, 8, 9]], axis=1, inplace=True)

# Currently, PowerBI puts column headers in row 1 (actually 2 but, apparently, pandas ignores the blank row 1).
# Be aware that python uses a 0 based counting system
raw.rename(raw.iloc[1], axis="columns", inplace=True)

# Those column names are still in our data. We need to remove them for the program to work.
# The number here should match the number above
raw = raw.drop(index=[1])

# Then we clean our data, removing observations with missing, invalid, or uninteresting responses.
raw.dropna(subset=['Therapy', 'Last Recorded Time', 'Department'], inplace=True)
raw = raw[raw['Department'].isin(flocations)]
raw = raw[raw['Therapy'].isin(Frequency)]

# Next, we make a month/year column for grouping, then we can get rid of the time column
raw['month'] = pd.DatetimeIndex(raw['Last Recorded Time']).to_period("M").to_timestamp()
raw.drop(raw.columns[[1]], axis=1, inplace=True)

# Last step working with the 'raw' data, renaming observations based on how we want them to look in the graph
raw['Department'].replace(flocations, inplace=True)
raw['Therapy'].replace(Frequency, inplace=True)

# We count all of the evaluations in each location for each month and save those in a new dataframe.
newSum = raw.value_counts(sort=False).reset_index(name='count')

# We don't need the old data frame hogging memory. We do need a list of all the months we're working with for later use
del raw
monthList = sorted(newSum['month'].unique())

# Create lists for storing the 'all rhodes' data as we iterate through the locations/months
for o in range(len(freqList)):
    lsName = 'All' + freqList[o]
    vars()[lsName] = []
    for q in range(len(monthList)):
        vars()[lsName].append(0)

# This entire section just formats the data the way we need it to make the graphs
for l in range(len(locationsList)):
    df = newSum.loc[newSum['Department'] == locationsList[l]].copy()
    df.drop(df.columns[[0]], axis=1, inplace=True)
    toDB = {}
    for n in range(len(freqList)):
        tempList = []
        lsName = 'All' + freqList[n]
        # If there were no evals for a given month/frequency/location, we put a zero
        for m in range(len(monthList)):
            if ((df['month'] == monthList[m]) & (df['Therapy'] == freqList[n])).any():
                tempList.extend(df.loc[(df['month'] == monthList[m]) & (df['Therapy'] == freqList[n]), ['count']]
                                .values[0])
            else:
                tempList.extend([0])
        toDB[freqList[n]] = tempList
        vars()[lsName] = [sum(r) for r in zip(vars()[lsName], tempList)]

    # Formatted data gets saved as a SQL database
    table2SQL = pd.DataFrame(toDB, index=monthList)
    table2SQL.to_sql(locationsList[l], a, if_exists='append')
    table2SQL['total'] = table2SQL.sum(axis=1)
    table2SQL = table2SQL.div(table2SQL['total'], axis=0)
    table2SQL.drop(table2SQL.columns[[6]], axis=1, inplace=True)
    table2SQL.index.names = ['month']
    table2SQL = table2SQL.mul(100).astype(int)
    table2SQL.to_sql(locationsList[l], e, if_exists='append')

toDBAll = {}
for p in range(len(freqList)):
    lsName = 'All' + freqList[p]
    toDBAll[freqList[p]] = vars()[lsName]
table2SQL = pd.DataFrame(toDBAll, index=monthList)
table2SQL.to_sql('Rhodes', a, if_exists='append')
table2SQL['total'] = table2SQL.sum(axis=1)
table2SQL = table2SQL.div(table2SQL['total'], axis=0)
table2SQL.drop(table2SQL.columns[[6]], axis=1, inplace=True)
table2SQL.index.names = ['month']
table2SQL = table2SQL.mul(100).astype(int)
table2SQL.to_sql('Rhodes', e, if_exists='append')

# Create and save charts
for m in range(len(locationsList)):
    chartTable = pd.read_sql_query('SELECT * FROM ' + locationsList[m] + ' ORDER BY month DESC LIMIT ' + num, e)
    chartTable.sort_values('month', inplace=True)
    chartTable.set_index('month', inplace=True)
    plt.rcParams["figure.figsize"] = (12, 6)
    plt.plot(chartTable.index, chartTable[freqList])
    plt.legend(freqList)
    plt.title(locationsList[m] + ' PT Treatment Frequency as % of All Evaluations')
#    plt.ylim(0, .9)
    plt.xticks(ticks=chartTable.index, labels=pd.DatetimeIndex(chartTable.index).strftime("%b %y"))
    plt.ylabel('%')
    for n in range(len(chartTable.index)):
        for o in range(len(freqList)):
            plt.text(n, chartTable.iloc[n, o], chartTable.iloc[n, o].astype(str) + '%', verticalalignment='bottom',
                     horizontalalignment='center')
#    plt.show()
    plt.savefig(chrtPath + '\\' + locationsList[m])
    plt.close()

chartTable = pd.read_sql_query('SELECT * FROM Rhodes ORDER BY month DESC LIMIT ' + num, e)
chartTable.sort_values('month', inplace=True)
chartTable.set_index('month', inplace=True)
plt.rcParams["figure.figsize"] = (12, 6)
plt.plot(chartTable.index, chartTable[freqList])
plt.legend(freqList)
plt.title('All Rhodes PT Treatment Frequency as % of All Evaluations')
#plt.ylim(0, .9)
plt.xticks(ticks=chartTable.index, labels=pd.DatetimeIndex(chartTable.index).strftime("%b %y"))
plt.ylabel('%')
for n in range(len(chartTable.index)):
    for o in range(len(freqList)):
        plt.text(n, chartTable.iloc[n, o], chartTable.iloc[n, o].astype(str) + '%', verticalalignment='bottom',
                 horizontalalignment='center')
#plt.show()
plt.savefig(chrtPath + '\All')
plt.close()