# Treatment Frequency Charts

## Description
This program was intended to automate the cleaning of and generation of visualizations from patient treatment data exported from powerBI. The physical therapy department of an academic medical center was exploring the possibility that they may be expending a lot resources to deliver low value care. They were creating monthly graphs of the data for several hospitals/units by hand and this program automated the process.

## Installation
While in use, I generated a self contained executable file that ran on the project leader's work laptop.

## Project status
This is no longer an active project. I am making the code available for posterity and anybody who may find it useful.

## License
This work is marked with [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)